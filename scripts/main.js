$('nav ul ul').localScroll({
  duration:500
});

$(window).scroll(function() {
  var screencenter = $(window).scrollTop() + 0.5 * $(window).height();
  var activeID;
  $('body>section').each(function(i) {
    if ($(this).offset().top <= screencenter) {
      activeID = $(this).attr('id');
    }
  });
  $('nav ul ul li.selected').removeClass('selected');
  $('a[href=#' + activeID + ']').parent().addClass('selected');
}).scroll();


// image gallery (magnific popup)
$(document).ready(function() {
	$('.gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Bild wird geladen',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1], // Will preload 0 - before current, and 1 after the current image
      tCounter: '%curr% von %total%' // markup of counter
		},
		image: {
			tError: '<a href="%url%">Das Bild</a> konnte nicht geladen werden.'
		}
	});
});
