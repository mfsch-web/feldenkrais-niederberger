.DEFAULT_GOAL := site
.PHONY: site clean

SITE = public
TOOLS = tools

PANDOC_VERSION = 2.11.1.1
pandoc = $(TOOLS)/pandoc-$(PANDOC_VERSION)/bin/pandoc
pandoc_cmd = $(pandoc) --from=markdown-auto_identifiers-smart --wrap=preserve
SASS_VERSION = 1.29.0
sass = $(TOOLS)/dart-sass/sass

content = $(patsubst content/%.md,/%,$(wildcard content/*.md))
$(info content: $(content))
downloads = $(notdir $(wildcard downloads/*))
$(info downloads: $(downloads))
static = $(patsubst static/%,/%,$(wildcard static/*))
scripts = main.js plugins.js vendor/jquery-1.10.2.min.js vendor/modernizr-2.6.2.min.js

paths = $(addsuffix .html,$(content)) \
    $(addprefix /downloads/,$(downloads)) \
    $(patsubst images/%,/img/%,$(wildcard images/*.jpg)) \
    $(addprefix /js/,$(scripts)) \
    /css/main.css \
    $(static)

site: $(addprefix $(SITE),$(paths))

clean:
	rm -rf $(SITE) $(TOOLS)

$(pandoc):
	mkdir -p $(TOOLS)
	wget -O $(TOOLS)/pandoc.tar.gz https://github.com/jgm/pandoc/releases/download/$(PANDOC_VERSION)/pandoc-$(PANDOC_VERSION)-linux-amd64.tar.gz
	tar -xzf $(TOOLS)/pandoc.tar.gz -C $(TOOLS)

$(sass):
	mkdir -p $(TOOLS)
	wget -O $(TOOLS)/dart-sass.tar.gz https://github.com/sass/dart-sass/releases/download/$(SASS_VERSION)/dart-sass-$(SASS_VERSION)-linux-x64.tar.gz
	tar -xzf $(TOOLS)/dart-sass.tar.gz -C $(TOOLS)

$(SITE)/img/%: images/%
	mkdir -p $(@D)
	cp $< $@

$(SITE)/js/%: scripts/%
	mkdir -p $(@D)
	cp $< $@

$(addprefix $(SITE),$(static)): $(SITE)/%: static/%
	mkdir -p $(@D)
	cp $< $@

$(SITE)/downloads/%: downloads/%
	mkdir -p $(@D)
	cp $< $@

$(SITE)/css/main.css: styles/main.scss styles/*.scss | $(sass)
	mkdir -p $(@D)
	$(sass) $< $@

.SECONDEXPANSION:

$(SITE)/%.html: content/%.md $$(sort $$(wildcard content/%/*)) $(wildcard templates/*.html) | $(pandoc)
	mkdir -p $(@D)
	$(pandoc_cmd) --template=templates/header.html $(subst /, --variable=nav-,/$*) $< > $@
	for f in $(filter %.md,$^); do $(pandoc_cmd) --template=templates/section.html $(subst /, --variable=nav-,/$*) $$f >> $@; done
	$(pandoc_cmd) --template=templates/footer.html $< >> $@
