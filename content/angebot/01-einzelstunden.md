---
title: Einzelstunden – Funktionale Integration®
slug: einzelstunden
image: bild-einzelstunden.jpg
---

Termine nach Vereinbarung  
Dauer ca. 60 Minuten  
Kosten Fr. 120.– (Reduktion nach Absprache möglich)

Wenn Sie Fragen haben oder einen Termin vereinbaren möchten, rufen Sie mich an oder schreiben Sie mir eine E-Mail.

Ich bin im EMR (Erfahrungsmedizinisches Register) und bei der ASCA (Schweizerische Stiftung für Komplementärmedizin) registriert. Viele Krankenkassen zahlen einen Beitrag aus der Zusatzversicherung an die Einzellektionen.

[Mehr zur Funktionalen Integration®](/feldenkrais#arbeitsweise)
