---
title: Gruppenstunden – Bewusstheit durch Bewegung®
slug: gruppenstunden
image: bild-gruppenstunden.jpg
---

Dienstag 12.30–13.30 Uhr  
Mittwoch 17.45–18.45 Uhr  
Donnerstag 11.30–12.30 Uhr  
Freitag 9.00–10.00 und 11.00–12.00 Uhr  
Fortlaufende Kurse, Einstieg jederzeit möglich.  
Auf Voranmeldung können Sie gerne in einer Gruppenstunde kostenlos schnuppern.

Kosten: Fr. 25.– pro Lektion  
Bei Bezahlung pro Quartal sind zwei Lektionen gratis.

Ich bin im EMR (Erfahrungsmedizinisches Register) und bei der ASCA (Schweizerische Stiftung für Komplementärmedizin) registriert.
Einige Krankenkassen zahlen einen Beitrag aus der Zusatzversicherung an die Gruppenlektionen.

Gruppenangebot für brustkrebsbetroffene Frauen, auf Anfrage.

[Daten der Gruppenkurse 2. Semester 2020](/downloads/feldenkrais-gruppenkurse-2020-2.pdf)

[Mehr zu Bewusstheit durch Bewegung®](/feldenkrais#arbeitsweise)
