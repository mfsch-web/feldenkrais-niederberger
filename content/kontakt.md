---
title-nav: Kontakt
title: Hier finden Sie mich
alternate-design: true
quote: Erst wenn wir wissen, was wir tun, können wir tun, was wir wollen.
contact:
  name: Marianne Niederberger-Katz
  title: Feldenkrais Lehrerin SFV
  street: Innere Margarethenstrasse 19
  zipcode: 4051
  town: Basel
  phone: 079 725 24 47
  map: lageplan.pdf
body: true # needed to prevent skipping file
---
