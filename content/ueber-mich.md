---
title: Marianne Niederberger
title-nav: Über mich
gallery:
  praxis-1-klein.jpg: praxis-1.jpg
  praxis-2-klein.jpg: praxis-2.jpg
  praxis-3-klein.jpg: praxis-3.jpg
  praxis-4-klein.jpg: praxis-4.jpg
image: bild-ueber-mich.jpg
---

«Beweglicher werden heisst lebendiger werden, körperlich, seelisch und geistig.» Dieses Zitat von Moshé Feldenkrais beschreibt sehr treffend meine Erfahrungen mit der Feldenkrais Methode® und motiviert mich gleichzeitig für meine Arbeit.

Ich freue mich, mit Menschen in unterschiedlichsten Lebenssituationen, jeden Alters und mit verschiedensten Anliegen zu arbeiten.

::: {.columns .alternate}
::: column

## Über mich

- 1962 geboren und aufgewachsen in Basel
- 1982–1985 Pflegeausbildung am St. Claraspital mit Diplom in allgemeiner Krankenpflege (AKP)
- Langjährige Erfahrung in der Pflege, unter anderem in der Onkologie und in der Palliativpflege

:::
::: column

- 1998–2001 Lehrgang in Erwachsenenbildung mit Abschluss SVEB 2
- 2003 Basisseminar Basale Stimulation in der Pflege
- 2004 Grundkurs Kinästhetik

:::
::: column

- 2008–2012 Ausbildung zur Feldenkrais-Lehrerin SFV bei Paul Rubin und Julie Casson Rubin in Basel
- 2011 Eröffnung meiner Praxis Krankenkassenanerkennung (EMR, ASCA)
- 2018 Branchenzertifikat KomplementärTherapie
- Zertifiziertes Mitglied des Schweizerischen Feldenkrais Verbands SFV

[Schweiz. Feldenkrais Verband SFV](http://www.feldenkrais.ch)

:::
:::
