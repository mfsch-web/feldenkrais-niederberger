---
description: "«Beweglicher werden heisst lebendiger werden, körperlich, seelisch und geistig.» Dieses Zitat von Moshé Feldenkrais beschreibt sehr treffend meine Erfahrungen mit der Feldenkrais Methode® und motiviert mich gleichzeitig für meine Arbeit."
alternate-design: true
quote: Beweglicher werden heisst lebendiger werden, körperlich, seelisch und geistig.
image: bild-einstieg.jpg
news:
  - description: Feldenkrais am Samstag im März
    file: feldenkrais-am-samstag-maerz-2025.pdf
  - description: Gruppenkurse 1. Semester 2025
    file: gruppenkurse-1-semester-2025.pdf
---

Willkommen in meiner Feldenkrais-Praxis in Basel!
Feldenkrais ist eine körperorientierte Lernmethode.
Sie wählt die Bewegung als Mittel, um körperliche und geistige Funktionen zu verbessern.
Kleine Veränderungen der Bewegungsmöglichkeiten und der Körperhaltung werden sich deutlich auf Ihr Befinden auswirken.
