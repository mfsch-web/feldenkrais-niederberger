---
title: "Eine behutsame und wirkungsvolle Methode"
slug: methode
quote: Aufmerksamkeit und nicht Willenskraft ist der Schlüssel, um eine persönliche Fähigkeit zu erwerben.
image: bild-methode.jpg
---

Jeder Mensch eignet sich im Lauf seiner persönlichen Geschichte ganz bestimmte körperliche und seelische Verhaltensweisen an. Diese Muster sind der Umgebung angepasst und tief im Nervensystem verwurzelt. Manche sind hilfreich und sinnvoll, andere schränken ein oder behindern gar. Die Feldenkrais Methode® bietet ein wirkungsvolles Lernfeld, um sich dieser Muster bewusst zu werden und sie behutsam zu verändern und zu erweitern.

Moshé Feldenkrais betont, dass sich das menschliche Sein und Handeln aus den Komponenten Bewegen, Wahrnehmen, Fühlen und Denken zusammensetzt. Diese vier durchdringen und beeinflussen sich gegenseitig. Verändert sich eine Komponente, führt dies unweigerlich zu Veränderungen der anderen drei. Durch eine Erweiterung der Bewegungsmöglichkeiten können wir somit auch unsere seelischen und geistigen Fähigkeiten erweitern – und unser Potenzial immer mehr ausschöpfen.

Die Feldenkrais Methode® vereint Erkenntnisse der Biomechanik, Hirnforschung, Pädagogik und Psychologie. Dank ihren bemerkenswerten Erfolgen wird sie in vielen Bereichen der Medizin und der Pädagogik eingesetzt.
