---
title: "Unterstützung in vielen Situationen"
slug: fuer-wen
quote: …das Unmögliche möglich machen, das Schwierige leicht und das Leichte angenehm
image: bild-fuer-wen.jpg
---

Die Feldenkrais Methode® eignet sich für Menschen in jedem Lebensalter und in den unterschiedlichsten Lebenssituationen. Insgesamt stärkt sie die Gesundheit und das Wohlbefinden, vermindert Beschwerden und unterstützt Heilungsprozesse.

::: columns
::: column

## Die Feldenkrais Methode® eignet sich, wenn Sie

- Ihre Beweglichkeit und Kraft erweitern wollen
- Selbstwahrnehmung und Körpergefühl verfeinern möchten
- Ihr psychisches Wohlbefinden und Ihr emotionales Gleichgewicht stärken möchten
- Ihre Fähigkeiten und Ihren Ausdruck als Musiker/in, Tänzer/in, Schauspieler/in oder Sportler/in weiterentwickeln möchten.

:::
::: column

## Sie unterstützt

- bei der Rehabilitation nach einem Unfall oder einer Krankheit
- beim Wiedererlangen der körperlichen Fähigkeiten nach einer Verletzung oder einem Schlaganfall
- beim Überwinden von Schonhaltungen und Bewegungseinschränkungen nach grossen Operationen
- beim Zurechtfinden mit einem veränderten Körperbild z.B. nach Amputationen
- beim Entwickeln eines positiven Körperempfindens z.B. bei Essstörungen.

:::
::: column

## Sie vermindert

- Verspannungen und Bewegungseinschränkungen
- Schmerzen unterschiedlichster Ursachen.

## Sie ist eine wertvolle Ergänzung

- zu einer Gesprächstherapie bei psychischen Problemen.

:::
:::
