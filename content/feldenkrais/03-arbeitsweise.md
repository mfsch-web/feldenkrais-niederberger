---
title: "Einzelarbeit oder in der Gruppe"
slug: arbeitsweise
quote: Lernen soll und kann ein angenehmes, ein wunderbares Erlebnis sein.
image: bild-arbeitsweise.jpg
---

Die Feldenkrais Methode® wird in zwei Formen unterrichtet:

::: segment

**Einzelstunden: «Funktionale Integration»** — In der Einzelarbeit gehe ich auf Ihre individuellen Bedürfnisse ein. Durch sorgfältige Berührungen und differenzierte Bewegungen können Sie einschränkende Gewohnheiten wahrnehmen. Gleichzeitig lernen Sie neue Bewegungsmöglichkeiten kennen. So erweitern Sie Ihren Handlungsspielraum und erleben Leichtigkeit, Stabilität und freiere Atmung. Oft vermindern sich auch Schmerzen. Moshé Feldenkrais nennt die Einzelarbeit «Funktionale Integration».

[Termine und Kosten](/angebot#einzelstunden)

:::

::: segment

**In der Gruppe: «Bewusstheit durch Bewegung»** — In einer kleinen Gruppe führe ich Sie mit Worten durch ungewohnte, ruhige Bewegungssequenzen. Die Bewegungen erweitern und verfeinern die Wahrnehmung des eigenen Körpers. Sie entdecken dabei mühelosere Bewegungen und erleben Ihre Kraft. So gelangen Sie zu einer besseren Koordination und mehr Leichtigkeit – nicht nur in den Bewegungen, sondern auch im Umgang mit den täglichen Anforderungen! Moshé Feldenkrais nennt die Gruppenlektionen «Bewusstheit durch Bewegung».

[Termine und Kosten](/angebot#gruppenstunden)

:::
