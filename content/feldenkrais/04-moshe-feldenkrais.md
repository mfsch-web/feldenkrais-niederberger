---
title: "Moshé Feldenkrais"
slug: moshe-feldenkrais
quote: Ich schaffe Bedingungen, bei denen Menschen herausfinden können, was sie brauchen, um ein besseres Leben zu leben.
image: bild-feldenkrais.jpg
image-extra: bild-moshe-feldenkrais.jpg
---

Die Feldenkrais Methode® wurde von Moshé Feldenkrais (1904–1984) entwickelt.
Feldenkrais war eine unkonventionelle, beeindruckende Persönlichkeit: ein Pionier im Denken und Handeln.
1904 in der Ukraine geboren, wanderte er mit vierzehn Jahren ohne seine Familie nach Palästina aus.
Später studierte er in Paris und wurde Ingenieur und Physiker.
Seine Leidenschaft für die innere Mechanik der Dinge, vor allem aber für die menschliche Bewegung, führte ihn zum Judo.
Als einer der ersten Europäer erlangte er den schwarzen Gürtel.

Eine Knieverletzung mit schlechter Prognose der Ärzte brachte ihn dazu, sein Bewegungsverhalten zu untersuchen.
Er heilte sich selbst und entwickelte in jahrzehntelanger Forschung die nach ihm benannte Methode.
Dabei schöpfte er aus Neurowissenschaften, Biomechanik, Entwicklungspsychologie, Pädagogik und anderen Quellen.
Ab 1949 lebte er wieder in Israel und widmete sich ausschliesslich der Praxis, Lehre und Verfeinerung seiner Methode.
Er leitete drei Ausbildungsgänge und gab so sein Wissen weiter.
1984 starb er, hochangesehen und weltbekannt, in Tel Aviv.
